#!/bin/bash

echo $0

pacman -Q ansible
if [ $? -ne 0 ] ; then
    sudo pacman -S ansible
fi

ansible-galaxy install -r requirements.yaml
ansible-playbook save_configs.yml
