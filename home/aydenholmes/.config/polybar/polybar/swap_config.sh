#!/bin/bash

FILE=~/.config/polybar/TOGGLE

if [ ! -f $FILE ]
then
    echo STANDARD > TOGGLE
fi

killall polybar || :

if [ $(cat $FILE) == "STANDARD" ]
then
    polybar extended &
    echo EXTENDED > ~/.config/polybar/TOGGLE

else
    polybar standard &
    echo STANDARD > ~/.config/polybar/TOGGLE
fi
