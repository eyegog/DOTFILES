#!/bin/bash

pacman -Q ansible
if [ $? -ne 0 ] ; then
    sudo pacman -S ansible
fi

ansible-galaxy install -r requirements.yaml
ansible-playbook deploy_configs.yml -K
